{ stdenv, buildGoPackage, pkgconfig, fetchgit, proj }:

buildGoPackage rec {
  name = "gics-${version}";
  version = "v1.0rc4";
  rev = "f4c8f18f7d7c61f3e30982f4f5263581426d8240";

  nativeBuildInputs = [pkgconfig];
  buildInputs = [proj];
  goPackagePath = "bitbucket.org/uwaploe/gics";
  subPackages = ["server" "survey" "reader" "monitor" "archiver" "watchxy"];
  preBuild = ''
    buildFlagsArray=("-tags" "release" "-ldflags" "-X main.Version=${version}")
  '';

  postFixup = ''
    mv -v $bin/bin/server $bin/bin/grid_svc
    mv -v $bin/bin/survey $bin/bin/survey_svc
    mv -v $bin/bin/reader $bin/bin/gps_reader
    mv -v $bin/bin/monitor $bin/bin/grid_monitor
    mv -v $bin/bin/archiver $bin/bin/gics_archiver
  '';

  src = fetchgit {
    inherit rev;
    url = "https://bitbucket.org/uwaploe/gics.git";
    sha256 = "18vz37fn14gk2f974ysr30rhfgkwgf4kg1nj7yd6wbpqznbhxf1z";
  };

  goDeps = ./deps.nix;

  meta = {
    description = "Grid survey software for ICEX";
    homepage = "https://bitbucket.org/uwaploe/gics/src";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.unix;
  };
}
