# GICS Software Packages

This repository contains a collection of
required [NixOS](http://nixos.org) software packages for the user account,
*sysop*, on the GICS computer.

## Installation

Log-in to the *sysop* account and clone this repository to the
`~/.config/nixpkgs` directory then run `nix-env` to install the software:

``` shellsession
mkdir -p ~/.config
cd ~/.config
git clone https://bitbucket.org/uwaploe/gics-nixpkgs.git nixpkgs
nix-env -r -i all-gics
```
