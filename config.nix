# Packages for the sysop account on GICS
with import <nixpkgs> {};
{
  packageOverrides = pkgs: with pkgs; rec {
    gogics = callPackage ./pkgs/go-gics {
      inherit buildGoPackage fetchgit proj pkgconfig;
    };
    # This will supply most of the Python packages that we need. The
    # rest will need to be installed by pip in a virtualenv
    pyenv = python36.withPackages (ps: with ps; [
      pip
      virtualenv
      pyserial
      click
      flask
      protobuf3_3
      redis
      gevent
      simplejson
      numpy
    ]);

    gics = buildEnv {
      name = "all-gics";
      paths = [
        pyenv
        stow
        inotify-tools
        proj
        go
        jq
        pkgconfig
        gpsbabel
        docker_compose
      ];
    };
  };
}